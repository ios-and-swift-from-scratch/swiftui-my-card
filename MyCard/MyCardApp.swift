//
//  MyCardApp.swift
//  MyCard
//
//  Created by Md. Ebrahim Joy on 13/1/24.
//

import SwiftUI

@main
struct MyCardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
